package com.company.base.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.BeforeClass;
import org.junit.Test;

import com.company.base.exception.InvalidOperationException;
import com.company.base.mother.OrderMother;
import com.company.base.mother.OrderPaymentMother;

public class OrderTest {

	static Order1 order;

	@BeforeClass
	public static void init() {
		order = OrderMother.regular().build();
	}

	@Test(expected = InvalidOperationException.class)
	public void invalidOrderNoItems() {

		Order1 order = OrderMother.withNoItems().build();

		order.validate();
	}

	@Test
	public void validOrderWithItems() {

		order.validate();

		assertTrue(order.itensCount() > 0);
	}

	@Test
	public void addVariousItems() {

		Order1 order = OrderMother.withNoItems().build();

		int initialItemsCount = order.itensCount();
		OrderItem murphysIrishBeer = OrderItem.builder().description("Murphy's Irish Stout").unitPrice(new BigDecimal("5.7")).quantity(19).build();
		OrderItem portherHouseIrishBeer = OrderItem.builder().description("Porterhouse").unitPrice(new BigDecimal("5.7")).quantity(5).build();

		order.addItems(murphysIrishBeer, portherHouseIrishBeer);

		assertTrue(initialItemsCount < order.itensCount());
	}

	@Test
	public void totalItensQuantity() {

		Order1 order = OrderMother.withNoItems().build();
		OrderItem heineken = OrderItem.builder().description("Heineken").unitPrice(new BigDecimal("2.9")).quantity(2).build();
		OrderItem miller = OrderItem.builder().description("Miller").unitPrice(new BigDecimal("2.9")).quantity(6).build();
		order.addItem(heineken);
		order.addItem(miller);

		int itensCount = order.itensCount();
		int testCount = 2;

		assertTrue(itensCount == testCount);
	}

	@Test(expected = InvalidOperationException.class)
	public void duplicateOrderItem() {

		OrderItem threeFloyds =  OrderItem.builder().description("Three Floyds").unitPrice(new BigDecimal("4.3")).quantity(3).build();
		OrderItem threeFloydsDuplicate = OrderItem.builder().description("Three Floyds").unitPrice(new BigDecimal("4.3")).quantity(3).build();

		order.addItem(threeFloyds);
		order.addItem(threeFloydsDuplicate);
	}

	@Test(expected = InvalidOperationException.class)
	public void orderItemInvalidPriceZero() {

		OrderItem threeFloyds = OrderItem.builder().description("Three Floyds").unitPrice(new BigDecimal("0")).quantity(3).build();
		
		order.addItem(threeFloyds);
	}

	@Test
	public void totalPriceValidCalculation() {

		Order1 order = OrderMother.withNoItems().build();
		OrderItem heineken = OrderItem.builder().description("Heineken").unitPrice(new BigDecimal("2.90")).quantity(2).build();
		OrderItem miller = OrderItem.builder().description("Miller").unitPrice(new BigDecimal("1.80")).quantity(6).build();
		
		order.addItem(heineken);
		order.addItem(miller);

		BigDecimal orderPrice = order.totalPrice();
		BigDecimal testPrice = new BigDecimal("4.70");

		assertTrue(orderPrice.equals(testPrice));
	}

	@Test
	public void totalPriceCalculationFail() {

		Order1 order = OrderMother.withNoItems().build();
		OrderItem heineken = OrderItem.builder().description("Heineken").unitPrice(new BigDecimal("2.90")).quantity(2).build();
		OrderItem miller = OrderItem.builder().description("Miller").unitPrice(new BigDecimal("1.80")).quantity(6).build();
		order.addItem(heineken);
		order.addItem(miller);

		BigDecimal orderPrice = order.totalPrice();
		BigDecimal testPrice = new BigDecimal("11.90");

		assertFalse(orderPrice.equals(testPrice));
	}

	@Test
	public void totalPriceRoundingAssurance() {

		Order1 order = OrderMother.withNoItems().build();
		OrderItem heineken = OrderItem.builder().description("Heineken").unitPrice(new BigDecimal("2.9987895")).quantity(2).build();
		OrderItem miller = OrderItem.builder().description("Miller").unitPrice(new BigDecimal("1.824323432")).quantity(6).build();
		order.addItem(heineken);
		order.addItem(miller);

		BigDecimal orderPrice = order.totalPrice();
		BigDecimal testPrice = new BigDecimal("4.82");

		assertTrue(orderPrice.equals(testPrice));
	}

	@Test
	public void totalPriceRoundingFail() {

		Order1 order = OrderMother.withNoItems().build();
		OrderItem heineken = OrderItem.builder().description("Heineken").unitPrice(new BigDecimal("2.9987895")).quantity(2).build();
		OrderItem miller = OrderItem.builder().description("Miller").unitPrice(new BigDecimal("1.824323432")).quantity(6).build();
		order.addItem(heineken);
		order.addItem(miller);

		BigDecimal orderPrice = order.totalPrice();
		BigDecimal testPrice = new BigDecimal("4.83");

		assertFalse(orderPrice.equals(testPrice));
	}

	@Test(expected = InvalidOperationException.class)
	public void invalidItemRemovalOneItemLeft() {

		Order1 order = OrderMother.withNoItems().build();
		OrderItem miller = OrderItem.builder().description("Miller").unitPrice(new BigDecimal("1.80")).quantity(6).build();

		order.removeItem(miller);
	}

	@Test(expected = InvalidOperationException.class)
	public void invalidItemRemovalNotAddedItem() {
		OrderItem murphysIrishBeer = OrderItem.builder().description("Murphy's Irish Stout").unitPrice(new BigDecimal("5.7")).quantity(19).build();
		OrderItem portherHouseIrishBeer = OrderItem.builder().description("Porterhouse").unitPrice(new BigDecimal("5.7")).quantity(5).build();
		
		order.addItem(murphysIrishBeer);
		order.removeItem(portherHouseIrishBeer);
	}

	@Test
	public void validItemRemoval() {

		int initialCount = order.itensCount();

		OrderItem frenchBeer = OrderItem.builder().description("Kronenbourg").unitPrice(new BigDecimal("6.2")).quantity(5).build();
		OrderItem germanBeer = OrderItem.builder().description("Spaten").unitPrice(new BigDecimal("2.4")).quantity(8).build();
		order.addItem(frenchBeer);
		order.addItem(germanBeer);

		order.removeItem(germanBeer);
		int testCount = initialCount + 1;

		assertTrue(testCount == initialCount + 1);
		assertTrue(initialCount < testCount);
	}

	@Test
	public void orderNotPaid() {

		boolean isOrderPaid = order.isPaid();

		assertFalse(isOrderPaid);
	}

	@Test
	public void paidOrder() {

		Order1 order = OrderMother.paidOrder().build();

		boolean isOrderPaid = order.isPaid();

		assertTrue(isOrderPaid);
	}
	
	@Test(expected = InvalidOperationException.class)
	public void duplicatePayment() {
		
		Order1 order = OrderMother.paidOrder().build();
		
		OrderPayment payment = OrderPaymentMother.concludedPayment().build();
		order.setPayment(payment);
	}

}
