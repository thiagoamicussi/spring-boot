package com.company.base.mother;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import com.company.base.model.Order1;
import com.company.base.model.OrderItem;
import com.company.base.model.OrderPayment;

public class OrderMother {

	public static Order1.Order1Builder regular() {

		Set<OrderItem> items = new HashSet<>();
		items.add(OrderItem.builder().description("Heineken").unitPrice(new BigDecimal("2.9")).quantity(2).build());
		items.add(OrderItem.builder().description("Miller").unitPrice(new BigDecimal("1.8")).quantity(6).build());
		items.add(OrderItem.builder().description("Corona Extra").unitPrice(new BigDecimal("2.4")).quantity(3).build());
		items.add(OrderItem.builder().description("Bud Light").unitPrice(new BigDecimal("2.7")).quantity(5).build());
		items.add(OrderItem.builder().description("Bush").unitPrice(new BigDecimal("2.1")).quantity(7).build());
		
		Order1.Order1Builder builder = Order1.builder().items(items);
		
		return builder;
	}

	public static Order1.Order1Builder withDuplicateItems() {

		OrderItem threeFloyds = OrderItem.builder().description("Three Floyds").unitPrice(new BigDecimal("4.3")).quantity(3).build();
		OrderItem threeFloydsDuplicate = OrderItem.builder().description("Three Floyds").unitPrice(new BigDecimal("4.3")).quantity(3).build();

		Set<OrderItem> items = new HashSet<>();
		items.add(threeFloyds);
		items.add(threeFloydsDuplicate);

		Order1.Order1Builder builder = Order1.builder().items(items);
		
		return builder;
	}

	public static Order1.Order1Builder withNoItems() {
		Order1.Order1Builder orderBuilder = Order1.builder();
		return orderBuilder;
	}

	public static Order1.Order1Builder paidOrder() {
		OrderPayment payment = OrderPaymentMother.concludedPayment().build();
		Order1.Order1Builder orderBuilder = Order1.builder().payment(payment);
		return orderBuilder;
	}
}
