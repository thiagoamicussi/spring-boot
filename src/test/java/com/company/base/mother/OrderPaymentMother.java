package com.company.base.mother;

import java.time.LocalDate;

import com.company.base.model.OrderPayment;
import com.company.base.model.PaymentState;

public class OrderPaymentMother {

	public static OrderPayment.OrderPaymentBuilder concludedPayment() {

		OrderPayment.OrderPaymentBuilder builder = OrderPayment.builder()
					.creditCardNumber("4556418751881461")
					.paymentState(PaymentState.CONCLUDED)
					.paymentDate(LocalDate.now());
		
		return builder;
	}

}
