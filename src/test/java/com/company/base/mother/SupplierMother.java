package com.company.base.mother;

import com.company.base.model.Supplier;

public class SupplierMother {

	public static Supplier.SupplierBuilder regular() {
		Supplier.SupplierBuilder builder = Supplier.builder().name("Supplier company")
											.address("Supplier company address");
		
		return builder;
	}

	public static Supplier.SupplierBuilder noName() {
		Supplier.SupplierBuilder builder = Supplier.builder().address("Supplier company address");
		
		return builder;
	}

	public static Supplier.SupplierBuilder nullName() {
		Supplier.SupplierBuilder builder = Supplier.builder().name(null)
				.address("Supplier company address");
		
		return builder;
	}

	public static Supplier.SupplierBuilder EmptyName() {
		Supplier.SupplierBuilder builder = Supplier.builder().name("")
				.address("Supplier company address");
		return builder;
	}

	public static Supplier.SupplierBuilder noAddress() {
		Supplier.SupplierBuilder builder = Supplier.builder().name("Supplier company");
		return builder;
	}

	public static Supplier.SupplierBuilder nullAdress() {
		Supplier.SupplierBuilder builder = Supplier.builder().name("Supplier company")
				.address(null);
		return builder;
	}

	public static Supplier.SupplierBuilder EmptyAddress() {
		Supplier.SupplierBuilder builder = Supplier.builder().name("Supplier company")
				.address("");
		return builder;
	}
}
