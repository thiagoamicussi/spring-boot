package com.company.base.resource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.company.base.model.Supplier;
import com.company.base.service.SupplierService;

@RunWith(SpringRunner.class)
@WebMvcTest(SupplierResource.class)
public class SupplierResourceTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean 
	private SupplierService service;
	
	@Autowired
	private SupplierResource resource;
	
	
	@Test
	public void whenSupplierInject_thenNotNull() {
		assertThat(resource).isNotNull();
	}
	
	@Test
	public void givenNoSuppliers_whenGetSuppliers_thenStatus400() throws Exception {
		
		mockMvc.perform(get("/providers")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
	}
	
	@Test
	public void givenNoSuppliers_whenGetSuppliers_thenStatus404() throws Exception {
		
		@SuppressWarnings("unchecked")
		Page<Supplier> foundPage = Mockito.mock(Page.class);
		
		when(service.loadAll(any(Pageable.class))).thenReturn(foundPage);
		when(foundPage.hasContent()).thenReturn(false);
		
		mockMvc.perform(get("/providers").param("page", "0").param("size", "1")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	public void givenSuppliers_whenGetSuppliers_thenStatus200() throws Exception {
		Supplier supplier1 = Supplier.builder().name("Nome teste 1").address("Address teste 1").build();
		Supplier supplier2 = Supplier.builder().name("Nome teste 2").address("Address teste 2").build();
		List<Supplier> suppliers = Arrays.asList(supplier1, supplier2);
		Page foundPage = new PageImpl<Supplier>(suppliers);
		
		when(service.loadAll(any(Pageable.class))).thenReturn(foundPage);
		
		mockMvc.perform(get("/providers").param("page", "0").param("size", "1")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
	}
	
}
