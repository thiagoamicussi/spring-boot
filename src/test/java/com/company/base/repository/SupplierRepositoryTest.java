package com.company.base.repository;

import static org.assertj.core.api.Assertions.assertThat;

import javax.validation.ConstraintViolationException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.company.base.JPAConfig;
import com.company.base.model.Supplier;
import com.company.base.mother.SupplierMother;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes=JPAConfig.class)
public class SupplierRepositoryTest {

	@Autowired
	SupplierRepository repository;
	
	@Rule
	public ExpectedException thrownException = ExpectedException.none();
	
	@Test
	public void givenValidSupplier_WhenSave_ThenSucess() {
		Supplier supplier = SupplierMother.regular().build();

		Supplier savedSupplier = repository.save(supplier);
		
		assertThat(savedSupplier.getName()).isEqualTo(supplier.getName());
	}
	
	@Test
	public void givenNullSupplier_WhenSave_ThenError() {
		
		thrownException.expect(InvalidDataAccessApiUsageException.class);
		thrownException.expectMessage("Target object must not be null");
		
		Supplier supplier = null;
		
		repository.save(supplier);
	}
	
	@Test
	public void givenNoName_WhenSave_ThenError() {
		
		thrownException.expect(ConstraintViolationException.class);
		thrownException.expectMessage("must not be empty");
		
		Supplier supplier = SupplierMother.noName().build();
		
		repository.save(supplier);
	}
	
	@Test
	public void givenNullName_whenSave_ThenError() {
		
		thrownException.expect(ConstraintViolationException.class);
		thrownException.expectMessage("must not be empty");
		
		Supplier supplier = SupplierMother.nullName().build(); 
		
		repository.save(supplier);
	}
	
	@Test
	public void givenEmptyName_whenSave_ThenError() {
		
		thrownException.expect(ConstraintViolationException.class);
		thrownException.expectMessage("must not be empty");
		
		Supplier supplier = SupplierMother.EmptyName().build(); 
		
		repository.save(supplier);
	}
	
		
	@Test
	public void givenNoAdress_WhenSave_ThenError() {
		
		thrownException.expect(ConstraintViolationException.class);
		thrownException.expectMessage("must not be empty");
		
		Supplier supplier = SupplierMother.noAddress().build();
		
		repository.save(supplier);
	}
	
	@Test
	public void givenNullAddress_whenSave_ThenError() {
		
		thrownException.expect(ConstraintViolationException.class);
		thrownException.expectMessage("must not be empty");
		
		Supplier supplier = SupplierMother.nullAdress().build(); 
		
		repository.save(supplier);
	}
	
	@Test
	public void givenEmptyAddress_whenSave_ThenError() {
		
		thrownException.expect(ConstraintViolationException.class);
		thrownException.expectMessage("must not be empty");
		
		Supplier supplier = SupplierMother.EmptyAddress().build(); 
		
		repository.save(supplier);
	}
}
