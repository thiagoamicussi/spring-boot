package com.company.base.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

final class Price {
	
	private Price() {
		throw new UnsupportedOperationException();
	}

	static BigDecimal valueOf (BigDecimal price) {
		return price.setScale(2, RoundingMode.HALF_UP);
	}
}
