package com.company.base.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;

import org.hibernate.envers.Audited;

import com.company.base.exception.InvalidOperationException;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName="of")
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Audited
@Entity
public class Order1 extends EntityAbstract {

	private static final String DUPLICATED_ITEM = "Item já existe";
	private static final String HAS_NO_ITEMS = "Não existem itens";
	private static final String INVALID_PRICE = "Preço inválido";
	private static final String IMPOSSIBLE_REMOVE = "Impossível remover item da lista";

	@JsonProperty
	private String address;

	@Builder.Default
	@JsonProperty
	private LocalDate confimationDate = LocalDate.now();

	@Enumerated(EnumType.STRING)
	@JsonProperty
	private OrderState state;

	@Builder.Default
	@JsonManagedReference
	@OneToMany(mappedBy = "order1", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<OrderItem> items = new HashSet<>();

	@JsonManagedReference
	@OneToOne(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private OrderPayment payment;

	@PrePersist
	protected void onCreate() {
		if (this.confimationDate == null)
			this.confimationDate = LocalDate.now();
	}

    private boolean isDuplicateItem(OrderItem item) {
        if (item == null)
            throw new InvalidOperationException("Order item can not be null");

		for (OrderItem currentItem : this.items) {
			if (currentItem.equals(item))
				return true;
		}

		return false;
	}

	private boolean isValidPrice(OrderItem orderItem) {
        if (orderItem == null)
            throw new InvalidOperationException("Order item can not be null");

		boolean isPriceZero = orderItem.getUnitPrice().compareTo(new BigDecimal("0.00")) == 0;
		return !isPriceZero;
	}

	/**
	 * Check object consistency
	 * @throws InvalidOperationException for inconsistent state.
	 */
	void validate() throws InvalidOperationException {
		boolean hasNoItems = this.items == null || this.items.size() == 0;
		if (hasNoItems)
			throw new InvalidOperationException(HAS_NO_ITEMS);
	}

	/**
	 * Count items order
	 * 
	 * @return items order quantity
	 */
	int itensCount() {
		return this.items.size();
	}

	/**
	 * Add new items to the order
	 * 
	 * @throws InvalidOperationException in case item is already in
	 *            the order or the price is invalid
	 */
	void addItem(OrderItem item) throws InvalidOperationException {
		if (isDuplicateItem(item))
			throw new InvalidOperationException(DUPLICATED_ITEM + ": " + item.getDescription());

		if (!isValidPrice(item))
			throw new InvalidOperationException(INVALID_PRICE);

		this.items.add(item);
	}

	/**
	 * Add various items at once
	 * 
	 * @param items orders items
	 */
	void addItems(OrderItem... items) {
	    if (items == null)
	        throw new InvalidOperationException("Can't add null item");

		for (OrderItem item : items) {
			addItem(item);
		}
	}

	/**
	 * @param orderItem to remove from Order
	 * @throws InvalidOperationException in case the orderItem is not in the Order
	 */
	void removeItem(OrderItem orderItem) throws InvalidOperationException {

		if (items.size() == 0) {
			throw new InvalidOperationException(HAS_NO_ITEMS);
		}

		OrderItem currentOrderItem;

		Iterator<OrderItem> iterator = this.items.iterator();
		while (iterator.hasNext()) {
			currentOrderItem = iterator.next();
			if (currentOrderItem.equals(orderItem)) {
				iterator.remove();
				return;
			}
		}

		throw new InvalidOperationException(IMPOSSIBLE_REMOVE);
	}

	/**
	 * Get total price for the order
	 */
	BigDecimal totalPrice() {
		BigDecimal orderPrice = Price.valueOf(BigDecimal.ZERO);

		for (OrderItem item : this.items) {
			orderPrice = orderPrice.add(item.getUnitPrice());
		}
		return Price.valueOf(orderPrice);
	}

	/**
	 * Check order payment
	 */
	boolean isPaid() {
		boolean isOrderPaid = false;
		if (this.payment != null) {
			isOrderPaid = true;
		}
		return isOrderPaid;
	}
	
	void setPayment(OrderPayment payment) {
		if (payment != null)
			throw new InvalidOperationException("Order is already paid");
	}
	
}
