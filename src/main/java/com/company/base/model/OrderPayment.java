package com.company.base.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName="of")
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Audited
@Entity
public class OrderPayment extends EntityAbstract {

	@JsonProperty
	private LocalDate paymentDate;

	@JsonProperty
	private String creditCardNumber;

	@Enumerated(EnumType.STRING)
	@JsonProperty
	private PaymentState paymentState;
	
	@JsonBackReference
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order1_id", referencedColumnName = "id")
	private Order1 order;

	@PrePersist
	protected void onCreate() {
		if (this.paymentState == null) {
			setDefaultPaymentState();
		}
	}

	private void setDefaultPaymentState() {
		this.paymentState = PaymentState.PENDING;
	}
	
	/**
	 * Get order id for the payment*
	 */
	public long getOrderId() {
		return this.order.getId();
	}
}
