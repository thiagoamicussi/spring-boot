package com.company.base.model;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName="of")
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Audited
@Entity
public class Supplier extends EntityAbstract {

	@NotEmpty
	@JsonProperty
	private String name;

	@NotNull
	@NotEmpty
	@JsonProperty
	private String address;
}
