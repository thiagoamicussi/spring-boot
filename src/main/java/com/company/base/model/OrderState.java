package com.company.base.model;

public enum OrderState {
	UNCONFIRMED("UNCONFIRMED"), CONFIRMED("CONFIRMED");
	
	private final String val;

    OrderState(String val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return val;
    }
}
