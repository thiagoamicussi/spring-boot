package com.company.base.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.company.base.exception.InvalidOperationException;
import com.company.base.model.OrderPayment;
import com.company.base.repository.OrderPaymentRepository;

@Service
public class OrderPaymentService {

	private OrderPaymentRepository orderPaymentRepository;

	@Autowired
	public OrderPaymentService(OrderPaymentRepository orderPaymentRepository) {
		this.orderPaymentRepository = orderPaymentRepository;
	}

	public OrderPayment save(OrderPayment orderPayment) {
		OrderPayment orderPaymentSaved = orderPaymentRepository.findByOrder(orderPayment.getOrder());
		if(orderPaymentSaved != null) {
			throw new InvalidOperationException("Order is already paid");
		}
		
		return orderPaymentRepository.save(orderPayment);
	}

	public Optional<OrderPayment> load(long id) {
		return orderPaymentRepository.findById(id);
	}
}
