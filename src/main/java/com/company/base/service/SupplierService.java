package com.company.base.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.company.base.model.Supplier;
import com.company.base.repository.SupplierRepository;

@Service
public class SupplierService {

	private SupplierRepository providerRepository;

	@Autowired
	public SupplierService(SupplierRepository providerRepository) {
		this. providerRepository = providerRepository;
	}

	public Supplier save(Supplier provider) {

		return providerRepository.save(provider);
	}

	public Optional<Supplier> load(long providerId) {
		return providerRepository.findById(providerId);
	}

	public Page<Supplier> loadAll(Pageable pageable) {
		return providerRepository.findAll(pageable);
	}

	public Supplier findByNameLikeQuery(String searchTerm) {
		return providerRepository.findByNameLikeQuery(searchTerm);
	}
}
