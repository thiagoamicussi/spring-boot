package com.company.base.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.company.base.model.Order1;
import com.company.base.model.OrderState;
import com.company.base.repository.OrderRepository;

@Service
public class OrderService {
	
	private OrderRepository orderRepository;

	@Autowired
	public OrderService(OrderRepository orderRepository) {
		this.orderRepository = orderRepository;
	}

	public Order1 save(Order1 order) {
		
		 return orderRepository.save(order);
	}
	
	public Optional<Order1> load(long id) {
		return orderRepository.findById(id);
	}
	
	public Order1 loadWithItems(long id) {
		return orderRepository.loadWithItems(id);
	}
	
	public List<Order1> findByState(OrderState orderState) {
		return orderRepository.findByState(orderState);
	}
	
	public List<Order1> findByConfirmationPeriod(LocalDate startDate, LocalDate endDate) {
		return orderRepository.findByConfirmationPeriod(startDate, endDate);
	}
}
