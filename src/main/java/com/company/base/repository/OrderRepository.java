package com.company.base.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.company.base.model.Order1;
import com.company.base.model.OrderState;

@Repository
public interface OrderRepository extends RevisionRepository<Order1, Long, Long>, JpaRepository<Order1, Long> {

	@Query("select o from Order1 o where o.state = :state")
	List<Order1> findByState(@Param("state") OrderState state);

	@Query("select o from Order1 o where o.confimationDate BETWEEN :startDate AND :endDate")
	List<Order1> findByConfirmationPeriod(@Param("startDate") LocalDate startDate,
			@Param("endDate") LocalDate endDate);

	@Query("select o from Order1 o join fetch o.items where o.id = :id")
	Order1 loadWithItems(@Param("id") long id);
}
