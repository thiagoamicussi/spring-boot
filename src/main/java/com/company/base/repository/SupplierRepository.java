package com.company.base.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

import com.company.base.model.Supplier;

@Repository
public interface SupplierRepository
		extends RevisionRepository<Supplier, Long, Long>, PagingAndSortingRepository<Supplier, Long> {

	@Query("select p from Supplier p where p.name like ?1")
	Supplier findByNameLikeQuery(String searchTerm);
}
