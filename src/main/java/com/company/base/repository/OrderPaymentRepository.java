package com.company.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

import com.company.base.model.Order1;
import com.company.base.model.OrderPayment;

@Repository
public interface OrderPaymentRepository
		extends RevisionRepository<OrderPayment, Long, Long>, JpaRepository<OrderPayment, Long> {
	
	OrderPayment findByOrder(Order1 order);

}
