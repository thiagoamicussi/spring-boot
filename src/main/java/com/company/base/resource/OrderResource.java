package com.company.base.resource;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.base.model.Order1;
import com.company.base.model.OrderState;
import com.company.base.service.OrderService;

@RestController
@RequestMapping("orders")
public class OrderResource implements ResourceAbstract {
	
	private OrderService orderService;

	@Autowired
	public OrderResource(OrderService orderService) {
		this.orderService = orderService;
	}
	
	@PostMapping
	public ResponseEntity<Object> save(@Valid @RequestBody Order1 order) {
		Order1 savedOrder = orderService.save(order);
		URI locationUri = getUri(savedOrder.getId());
		return ResponseEntity.created(locationUri).build();
	}
	
	@GetMapping(value = "/withItems/{id}")
	public ResponseEntity<Order1> loadAll(@PathVariable("id") long id) {
		Order1 order = orderService.loadWithItems(id);
		return (order == null) ? ResponseEntity.notFound().build() :
				new ResponseEntity<>(order, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Order1> load(@PathVariable("id") long id) {
		Optional<Order1> optionalOrder = orderService.load(id);
		return optionalOrder.isPresent() ? new ResponseEntity<>(optionalOrder.get(), HttpStatus.OK) :
				ResponseEntity.notFound().build();
	}
	
	@GetMapping(value = "/state/{state}")
	public ResponseEntity<List<Order1>> getByState(@PathVariable("state") OrderState orderState) {
		
		List<Order1> orders = orderService.findByState(orderState);

		return orders.size() == 0 ? ResponseEntity.notFound().build() :
			new ResponseEntity<>(orders, HttpStatus.OK);
	}
	
	@GetMapping(value = "/confirmation-date/from/{startDate}/to/{endDate}")
	public ResponseEntity<List<Order1>> getByState(
			@PathVariable("startDate") String startDate,
			@PathVariable("endDate") String endDate) {
		
		LocalDate startDateLocalDate = LocalDate.parse(startDate);
		LocalDate endDateLocalDate = LocalDate.parse(endDate);
		
		List<Order1> orders = orderService.findByConfirmationPeriod(startDateLocalDate, endDateLocalDate);
		if(orders.size() == 0) {
			return ResponseEntity.notFound().build();
		}
		
		return new ResponseEntity<>(orders, HttpStatus.OK);
	}

}
