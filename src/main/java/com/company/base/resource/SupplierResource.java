package com.company.base.resource;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.base.model.Supplier;
import com.company.base.service.SupplierService;

@RestController
@RequestMapping("providers")
public class SupplierResource implements ResourceAbstract {

	private SupplierService providerService;

	@Autowired
	public SupplierResource(SupplierService providerService) {
		this.providerService = providerService;
	}
	
	@PostMapping
	public ResponseEntity<Object> save(@Valid @RequestBody Supplier provider) {

		Supplier savedProvider = providerService.save(provider);

		URI locationUri = getUri(savedProvider.getId());
		return ResponseEntity.created(locationUri).build();
	}

	@GetMapping(value = "/name/{name}")
	public ResponseEntity<Supplier> load(@PathVariable("name") String name) {

		Supplier provider = providerService.findByNameLikeQuery(name);
		if (provider == null) {
			return ResponseEntity.notFound().build();
		}

		return new ResponseEntity<>(provider, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<Page<Supplier>> loadAll(Pageable pageable) {
		Page<Supplier> supplierPage = providerService.loadAll(pageable);
		if (!supplierPage.hasContent()) {
			return ResponseEntity.notFound().build();
		}
		
		return new ResponseEntity<>(supplierPage, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Supplier> load(@PathVariable("id") long id) {

		Optional<Supplier> optionalProvider = providerService.load(id);

		return optionalProvider.isPresent() ? new ResponseEntity<>(optionalProvider.get(), HttpStatus.OK) :
				ResponseEntity.notFound().build();
	}

	@PutMapping(value = "/{id}")
	public ResponseEntity<Object> update(@Valid @RequestBody Supplier provider, @PathVariable("id") long id) {
		Optional<Supplier> optionalProvider = providerService.load(id);
		if (!optionalProvider.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		provider.setId(id);
		providerService.save(provider);

		return ResponseEntity.ok().build();
	}

}
