package com.company.base.resource;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.base.model.Order1;
import com.company.base.model.OrderPayment;
import com.company.base.service.OrderPaymentService;
import com.company.base.service.OrderService;

@RestController
@RequestMapping("payments")
public class OrderPaymentResource implements ResourceAbstract {

	@Autowired
	OrderPaymentService orderPaymentService;

	@Autowired
	private OrderService orderService;

	@PostMapping
	public ResponseEntity<Object> save(@Valid @RequestBody OrderPayment orderPayment) {

		Optional<Order1> optionalOrder = orderService.load(orderPayment.getOrderId());
		if (!optionalOrder.isPresent()) {
			return ResponseEntity.badRequest().body(errorFromEvelope("Invalid order"));
		}

		OrderPayment savedOrderPayment = orderPaymentService.save(orderPayment);

		URI locationUri = getUri(savedOrderPayment.getId());
		return ResponseEntity.created(locationUri).build();
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<OrderPayment> load(@PathVariable("id") long id) {
		Optional<OrderPayment> optionalOrderPayment = orderPaymentService.load(id);
		return optionalOrderPayment.isPresent() ? new ResponseEntity<>(optionalOrderPayment.get(), HttpStatus.OK) :
				ResponseEntity.notFound().build();
	}
}
