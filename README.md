# Project characteristics and technologies:

* Unit Test (JUNIT)
    * Builder pattern
    * Object Mother Pattern
    * Repository Test
    * REST controller Test
    * Mocking with Mockito
* Spring boot
* Lombok
* Hibernate
	* JPA Auditing
	* JPA Repositories
* REST API
    * API Tests (Postman)
    * Swagger docummentation
* Database H2
* Maven
	* Google jib container tools plugin
